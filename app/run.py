from flask import Flask, render_template, request, jsonify
from whois import whois

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/whois', methods=['POST'])
def whoiss():
    r = request.form['domain'] #request Flask
    w = whois(r)
    return jsonify(w)


if __name__ == '__main__':
   app.run(debug = True, host='0.0.0.0')
